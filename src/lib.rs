// SPDX-License-Identifier: GPL-3.0-or-later
#![warn(missing_docs)]
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::missing_errors_doc)]
#![warn(clippy::missing_panics_doc)]
#![warn(unused_must_use)]

//! # libchors
//!
//! Library for chors.

/// For lexical analysis and tokenization.
pub mod tokenizer;

/// For creating symbol trees.
pub mod symbolizer;

/// Unit tests.
mod test;
