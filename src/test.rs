#[cfg(test)]
mod tests {
    use crate::{
        symbolizer::{debug::print_symbol_tree, SymbolTree, SyntaxError},
        tokenizer::tokenize,
    };
    use lazy_static::lazy_static;

    lazy_static! {
        static ref GOOD_CODE: Vec<&'static str> = vec![
            "CHORE (cleaning) vacuum EVERY 2 weeks OFFSET 0 weeks FOR 15 minutes.",
            "CHORE (cleaning) vacuum EVERY 2w OFFSET 0 FOR 15m.",
            "CHORE (cleaning) vacuum BIWEEKLY EARLY FOR 15m.",
        ];
        static ref BAD_CODE: Vec<&'static str> = vec![
            r#"CHORE (cleaning) vacuum EVERY 2w OFFSET 0 FOR 15m.
            # Let's make a syntax error:
            CHORE
            (
                clea
                ning)
            vacuum BIWEEKLY EARLY FOR 15m."#
        ];
    }

    #[test]
    fn tokenize_good_code() {
        for code in GOOD_CODE.iter() {
            let _tokens = tokenize(code);
            // print_token_table(_tokens, true);
        }
    }

    #[test]
    fn tokenize_bad_code() {
        for code in GOOD_CODE.iter() {
            let _tokens = tokenize(code);
            // print_token_table(_tokens, true);
        }
    }

    #[test]
    fn symbolize_good_code() -> Result<(), SyntaxError> {
        for code in GOOD_CODE.iter() {
            let tokens = tokenize(code);
            let mut tree = SymbolTree::new();
            tree.push_tokens(tokens)?;
            print_symbol_tree(&tree);
        }
        Ok(())
    }

    #[test]
    fn symbolize_bad_code() -> Result<(), SyntaxError> {
        for code in BAD_CODE.iter() {
            let tokens = tokenize(code);
            let mut tree = SymbolTree::new();
            let res = tree.push_tokens(tokens);
            assert!(res.is_err());
            // println!("{}", res.err().unwrap().to_string().red());
        }
        Ok(())
    }
}
