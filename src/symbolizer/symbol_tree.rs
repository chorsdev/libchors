// SPDX-License-Identifier: GPL-3.0-or-later
use super::{errors::SyntaxError, Symbol, SymbolKind};
use crate::{
    symbolizer::symbol_expectations,
    tokenizer::{Token, TokenKind},
};
use common_macros::hash_map;
use itertools::Itertools;
use std::{
    cell::{Ref, RefCell, RefMut},
    collections::HashMap,
    iter::once,
};

/// A tree composed of [Symbol]s, the result of parsing a vector of [Token]s.
pub struct SymbolTree {
    /// Symbols that compose the tree, indexed by symbol ID.
    pub symbols: HashMap<usize, RefCell<Symbol>>,

    /// Edges from parents to their children, indexed by parent ID.
    pub children: HashMap<usize, Vec<usize>>,

    /// Edges from children to their parents, indexed by child ID.
    pub parents: HashMap<usize, usize>,

    /// ID of the last created symbol.
    last_id: usize,
}

impl Default for SymbolTree {
    fn default() -> Self {
        Self::new()
    }
}

/// Creates the hashmap used by [SymbolTree]'s `symbols` field.
fn setup_map() -> HashMap<usize, RefCell<Symbol>> {
    HashMap::new()
}

impl SymbolTree {
    /// Creates a new [SymbolTree].
    pub fn new() -> Self {
        Self {
            symbols: hash_map!(with setup_map(); insert {
                0 => RefCell::new(Symbol::new(0, SymbolKind::Root)),
            }),
            children: HashMap::new(),
            parents: HashMap::new(),
            last_id: 0,
        }
    }

    /// Parses [Token]s into [Symbol]s and adds them to the tree.
    ///
    /// # Errors
    ///
    /// Returns a [SyntaxError] if the `tokens` argument contains malformed code.
    pub fn push_tokens(
        &mut self,
        tokens: impl IntoIterator<Item = Token>,
    ) -> Result<(), SyntaxError> {
        let tree = RefCell::new(self);
        let iter = tokens
            .into_iter()
            .map(Some)
            .chain(once(None))
            .tuple_windows();

        for (opt_token, opt_next_token) in iter {
            let token = match opt_token {
                Some(t) => t,
                None => break,
            };
            let next_token_kind = opt_next_token.map(|t| t.kind);
            let last_id = {
                let tree_borrowed = tree.borrow();
                let last = tree_borrowed.last().borrow_mut();
                let takes_token = last
                    .takes_token(&token.kind, &next_token_kind)
                    .map_err(|e| SyntaxError::new(token.clone(), e))?;
                let last_id = last.id;
                drop(last);
                drop(tree_borrowed);
                if takes_token {
                    tree.borrow_mut().give_token_to_symbol(last_id, token);
                    continue;
                }
                last_id
            };

            if !try_fulfill_expectation_path(&tree, last_id, &token, &next_token_kind) {
                // Symbols must close if they refuse a token
                tree.borrow_mut().close_symbol(last_id);
                try_give_parent(&tree, last_id, token.clone(), &next_token_kind);
            };
        }

        Ok(())
    }

    /// Returns the [Symbol] associated with `last_id`.
    fn last(&self) -> &RefCell<Symbol> {
        self.symbols.get(&self.last_id).expect("no root")
    }

    /// Immutably borrows a [Symbol] with the given ID.
    fn borrow_symbol(&self, symbol_id: &usize) -> Ref<Symbol> {
        self.symbols
            .get(symbol_id)
            .expect("symbol not found")
            .borrow()
    }

    /// Mutably borrows a [Symbol] with the given ID.
    fn borrow_mut_symbol(&mut self, symbol_id: &usize) -> RefMut<Symbol> {
        self.symbols
            .get(symbol_id)
            .expect("symbol not found")
            .borrow_mut()
    }

    /// Returns the kinds of the children of the [Symbol] with the given ID.
    fn children_kinds(&self, id: usize) -> Vec<SymbolKind> {
        match self.children.get(&id) {
            Some(children) => children
                .iter()
                .map(|child_id| self.borrow_symbol(child_id).kind.clone())
                .collect(),
            None => Vec::new(),
        }
    }

    /// Returns the ID to use for the next [Symbol].
    fn next_id(&self) -> usize {
        self.symbols.len()
    }

    /// Creates a new [Symbol] and adds it to the tree.
    ///
    /// # Returns
    ///
    /// ID of the new [Symbol].
    fn new_symbol(&mut self, kind: SymbolKind, parent_id: usize) -> usize {
        self.last_id = self.next_id();
        self.symbols
            .insert(self.last_id, RefCell::new(Symbol::new(self.last_id, kind)));
        self.connect(parent_id, self.last_id);
        self.last_id
    }

    /// Creates edges between the parent and child [Symbol]s based on the provided IDs.
    fn connect(&mut self, parent_id: usize, child_id: usize) {
        self.parents.insert(child_id, parent_id);
        self.children
            .entry(parent_id)
            .or_insert(Vec::new())
            .push(child_id);
    }

    /// Closes a symbol and its children recursively. Closed symbols cannot accept new tokens.
    fn close_symbol(&mut self, symbol_id: usize) {
        let mut symbol = self.borrow_mut_symbol(&symbol_id);
        symbol.closed = true;
        drop(symbol);

        let children = match self.children.get(&symbol_id) {
            Some(c) => c,
            None => return,
        }
        .clone();

        for child_id in children {
            self.close_symbol(child_id);
        }
    }

    /// Pushes the token to the symbol, and closes it where applicable.
    ///
    /// # Warning
    ///
    /// This does not check if the symbol can take the token. It is assumed that this check has
    /// already been performed.
    fn give_token_to_symbol(&mut self, symbol_id: usize, token: Token) {
        let mut symbol = self.borrow_mut_symbol(&symbol_id);
        symbol.tokens.push(token);
    }
}

/// Recursively searches up the tree, looking for a [Symbol] that will accept the [Token].
///
/// # Panics
///
/// Panics if the root of the tree is reached without giving the [Token] to any [Symbol].
fn try_give_parent(
    tree: &RefCell<&mut SymbolTree>,
    child_id: usize,
    token: Token,
    next_token_kind: &Option<TokenKind>,
) {
    let opt_parent_id = {
        let tree_borrowed = tree.borrow();
        tree_borrowed.parents.get(&child_id).cloned()
    };

    match opt_parent_id {
        Some(parent_id) => {
            {
                let takes_token = {
                    match tree.borrow().symbols.get(&parent_id) {
                        Some(symbol) => symbol
                            .borrow()
                            .takes_token(&token.kind, next_token_kind)
                            .unwrap_or(false),
                        None => unreachable!("symbol not found"),
                    }
                };

                if takes_token {
                    tree.borrow_mut().give_token_to_symbol(parent_id, token);
                    return;
                } else if try_fulfill_expectation_path(tree, parent_id, &token, next_token_kind) {
                    return;
                }
            };

            try_give_parent(tree, parent_id, token, next_token_kind)
        }
        None => {
            panic!("reached root of tree without giving token: {token}")
        }
    }
}

/// Recursively determines what kinds of [Symbol]s the symbol with provided ID expects, based on
/// the provided token. If a suitable path is found, then the symbols are created.
///
/// For example, a `STATEMENT` symbol cannot take a `KEYWORD` [Token], but it can take a `CLAUSE`
/// symbol, which can take the `KEYWORD` [Token]. There is no limit to the depth of the path, as it
/// is based on the symbols' expectations. See: [`symbol_expectations`]
///
/// This occurs when a symbol refuses a token.
///
/// # Parameters
///
/// * `tree` - The tree to which the created symbols should be connected.
/// * `symbol_id` - ID of the symbol that refused the token.
/// * `token` - The token that was refused.
///
fn try_fulfill_expectation_path(
    tree: &RefCell<&mut SymbolTree>,
    symbol_id: usize,
    token: &Token,
    next_token_kind: &Option<TokenKind>,
) -> bool {
    /// # Internal
    ///
    /// See [`try_fulfill_expectation_path`]
    fn find_expectation_path(
        expectations: &[SymbolKind],
        token_kind: &TokenKind,
    ) -> Option<Vec<SymbolKind>> {
        /// # Internal
        ///
        /// See [`try_fulfill_expectation_path`]
        fn _recur(
            symbol_order: Vec<SymbolKind>,
            token_kind: &TokenKind,
        ) -> Option<Vec<SymbolKind>> {
            let symbol_kind = &symbol_order[symbol_order.len() - 1];

            if symbol_kind.takes_token_naive(token_kind) {
                return Some(symbol_order);
            }

            // find first expectation for a potential new child
            let expectations = match symbol_expectations(symbol_kind) {
                Some(x) => x.0[0],
                None => return None,
            };

            let mut possible_paths: Vec<Vec<SymbolKind>> = expectations
                .iter()
                .filter_map(|x| {
                    let mut next_symbol_order = symbol_order.clone();
                    next_symbol_order.push(x.clone());
                    _recur(next_symbol_order, token_kind)
                })
                .collect();

            match possible_paths.len() {
                1 => Some(possible_paths[0].clone()),
                0 => None,
                _ => {
                    possible_paths.sort_by_key(|a| a.len());
                    Some(possible_paths[0].clone())
                }
            }
        }

        let possible_paths: Vec<Vec<SymbolKind>> = expectations
            .iter()
            .filter_map(|x| _recur(vec![x.clone()], token_kind))
            .collect();

        match possible_paths.len() {
            1 => Some(possible_paths[0].clone()),
            0 => None,
            _ => panic!("___too many paths found___"),
        }
    }

    let opt_path = {
        let tree_borrowed = tree.borrow();
        let symbol = match tree_borrowed.symbols.get(&symbol_id) {
            Some(s) => s,
            None => unreachable!("symbol not found"),
        };
        let symbol_borrowed = symbol.borrow();
        let children_kinds = tree_borrowed.children_kinds(symbol_id);
        let expectation = symbol_borrowed.expectation(children_kinds);

        if let Some(x) = expectation {
            find_expectation_path(x, &token.kind)
        } else {
            None
        }
    };

    if let Some(path) = opt_path {
        let mut parent_id = symbol_id;
        let mut tree_mut = tree.borrow_mut();
        for kind in path {
            parent_id = tree_mut.new_symbol(kind, parent_id);
        }
        let res_takes_token = tree_mut
            .borrow_symbol(&parent_id)
            .takes_token(&token.kind, next_token_kind);

        if let Ok(takes_token) = res_takes_token {
            if takes_token {
                tree_mut.give_token_to_symbol(parent_id, token.clone());
            } else {
                unreachable!("incongruency between takes_token_naive and takes_token");
            }
        };
        return true;
    }
    false
}
