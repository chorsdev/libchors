// SPDX-License-Identifier: GPL-3.0-or-later
use crate::tokenizer::Token;
use std::{
    error::Error,
    fmt::{self, Debug},
};

/// Indicates malformed code.
pub struct SyntaxError {
    /// The offending token.
    token: Token,

    /// A bit of help or information about the malformation.
    help: String,
}

impl SyntaxError {
    /// Creates a new SyntaxError.
    pub fn new(token: Token, help: String) -> SyntaxError {
        SyntaxError { token, help }
    }
}

impl Error for SyntaxError {}

impl fmt::Debug for SyntaxError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Syntax error: `{}` @ ln {} col {}, {}",
            self.token.lexeme.escape_debug(),
            self.token.line,
            self.token.column,
            self.help
        )
    }
}

impl fmt::Display for SyntaxError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        <SyntaxError as Debug>::fmt(self, f)
    }
}
