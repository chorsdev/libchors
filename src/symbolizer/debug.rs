// SPDX-License-Identifier: GPL-3.0-or-later
use super::SymbolTree;
use crate::{symbolizer::Symbol, tokenizer::TokenKind};
use colored::Colorize;
use std::{cell::RefCell, cmp::max};

/// Pretty-prints a [SymbolTree] to aid in debugging.
///
/// # Panics
///
/// Panics if the concept of mathematics breaks down in an global anomalous event.
pub fn print_symbol_tree(tree: &SymbolTree) {
    let tree = RefCell::new(tree);

    /// Pretty-prints an individual [Symbol].
    fn print_symbol(tree: RefCell<&SymbolTree>, symbol: &RefCell<Symbol>, depth: usize) {
        print!(
            "{}{}{}",
            "    ".repeat(max(depth.saturating_sub(1), 0)).dimmed(),
            "    ".repeat(if depth > 0 { 1 } else { 0 }).dimmed(),
            symbol.borrow(),
        );

        if !symbol.borrow().tokens.is_empty() {
            let mut i_whitespace = 0;
            let mut i_text = 0;
            print!("{}", ": ".dimmed());

            for token in symbol.borrow().tokens.iter() {
                let colored = match token.kind {
                    TokenKind::Newline => {
                        i_whitespace += 1;
                        let newline = token.lexeme.escape_debug().to_string();
                        match i_whitespace % 2 {
                            0 => newline.black().on_red(),
                            1 => newline.black().on_cyan(),
                            _ => unreachable!(),
                        }
                    }
                    TokenKind::Whitespace => {
                        i_whitespace += 1;
                        let whitespace = "●".repeat(token.lexeme.len());
                        match i_whitespace % 2 {
                            0 => whitespace.red().dimmed(),
                            1 => whitespace.cyan().dimmed(),
                            _ => unreachable!(),
                        }
                    }
                    _ => {
                        i_text += 1;
                        let text = token.lexeme.escape_debug().to_string();
                        match i_text % 2 {
                            0 => text.yellow(),
                            1 => text.magenta(),
                            _ => unreachable!(),
                        }
                    }
                };

                print!("{}", colored);
            }
        }

        println!();

        if let Some(children) = tree.borrow().children.get(&symbol.borrow().id) {
            children
                .iter()
                .filter_map(|child_id| tree.borrow().symbols.get(child_id))
                .for_each(|child| print_symbol(tree.clone(), child, depth + 1));
        }
    }

    let tree_borrowed = tree.borrow();
    match tree_borrowed.symbols.get(&usize::default()) {
        Some(root) => print_symbol(tree.clone(), root, 0),
        None => panic!("cannot print empty symbol tree"),
    }
}

/// Pretty-prints a list of symbols in a [SymbolTree] to aid in debugging.
pub fn print_symbol_list(tree: &SymbolTree) {
    println!("SYMBOLS:");
    for (i, symbol) in tree.symbols.iter() {
        println!("{i}: {}", symbol.borrow());
    }
    println!("\nPARENTS:");
    for (i, parent) in tree.parents.iter() {
        println!("{i}: [{parent}]");
    }
    println!("\nCHILDREN:");
    for (i, children) in tree.children.iter() {
        println!(
            "{i}: [{}]",
            children
                .iter()
                .map(|c| format!("{}", c))
                .collect::<Vec<String>>()
                .join(", ")
        );
    }
}
