// SPDX-License-Identifier: GPL-3.0-or-later
use crate::tokenizer::{Token, TokenKind};
use colored::Colorize;
use convert_case::{Case, Casing};
use std::fmt::{self, Display};
use strum::{EnumString, EnumVariantNames};

/// Kinds of [Symbol]s.
#[derive(
    Debug, Clone, PartialEq, Eq, Hash, strum_macros::Display, EnumString, EnumVariantNames,
)]
pub enum SymbolKind {
    /// The root of a tree. This would typically mean the root of a file.
    Root,

    /// Comments, notated with `#`.
    Comment,

    /// A group of clauses.
    Statement,

    /// A keyword and an argument. Similar to a function call in other languages.
    Clause,

    /// A name that determines the kind of a clause.
    Keyword,

    /// Literal values or an array of literal values in a clause.
    Argument,

    /// A group of values.
    Array,

    /// A string of text. Quotes optional.
    String,

    /// A number, optionally including a unit.
    Scalar,
}

impl SymbolKind {
    /// Determines if a symbol of the given kind will take a token of the given
    /// kind **when it has no children**.
    pub fn takes_token_naive(&self, token_kind: &TokenKind) -> bool {
        match self {
            SymbolKind::Comment => token_kind == &TokenKind::Comment,
            SymbolKind::Clause => token_kind == &TokenKind::Whitespace,
            SymbolKind::Keyword => token_kind == &TokenKind::Keyword,
            SymbolKind::Array => token_kind == &TokenKind::ArrayOpen,
            SymbolKind::Scalar => token_kind == &TokenKind::Number,
            SymbolKind::String => matches!(
                token_kind,
                TokenKind::QuoteSingle | TokenKind::QuoteDouble | TokenKind::Text
            ),
            _ => false,
        }
    }
}

/// Used to determine if the last expectation of a [Symbol] should repeat. For example, `ROOT`
/// symbols can contain any number of statements. Note that this is not the only factor in this
/// decision. See: `expectation` in `impl Symbol`
pub enum Bounds {
    /// Indicates that the last expectation of a symbol should not repeat.
    Closed,

    /// Indicates that the last expectation of a symbol should repeat.
    Open,
}

/// Determines what kind(s) of child [Symbol]\(s) a symbol expects.
///
/// # Returns
///
/// An tuple containing the the symbol's expectations.
///
/// * `0` - An array slice of the symbol's expected child kinds, in the order they are expected.
///   The values in this array slice are also array slices, because there may be multiple kinds that
///   would be accepted.
/// * `1` - The [Bounds] of the symbol's expectations.
pub fn symbol_expectations(
    kind: &SymbolKind,
) -> Option<(&'static [&'static [SymbolKind]], Bounds)> {
    match kind {
        SymbolKind::Root => Some((
            &[&[SymbolKind::Statement, SymbolKind::Comment]],
            Bounds::Open,
        )),
        SymbolKind::Statement => Some((&[&[SymbolKind::Clause]], Bounds::Open)),
        SymbolKind::Clause => Some((
            &[&[SymbolKind::Keyword], &[SymbolKind::Argument]],
            Bounds::Open,
        )),
        SymbolKind::Argument => Some((
            &[&[SymbolKind::String, SymbolKind::Scalar, SymbolKind::Array]],
            Bounds::Closed,
        )),
        SymbolKind::Array => Some((&[&[SymbolKind::String, SymbolKind::Scalar]], Bounds::Open)),
        _ => None,
    }
}

/// Represents a symbol, which may contain tokens and/or child symbols. Used as nodes in a symbol
/// tree.
pub struct Symbol {
    /// ID of the symbol. Used to look up the symbols edges.
    pub id: usize,

    /// Symbol kind, aka type.
    pub kind: SymbolKind,

    /// The tokens contained in this symbol. For example, a `STRING` symbol would contain a `TEXT`
    /// token.
    pub tokens: Vec<Token>,

    /// Indicates if the symbol can take additional tokens.
    pub closed: bool,
}

impl Display for Symbol {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let kind_str = self.kind.to_string().to_case(Case::UpperSnake);
        write!(
            f,
            "{} {kind_str}",
            if self.closed {
                format!("[{}]", self.id).green()
            } else {
                format!("[{}]", self.id).dimmed()
            },
        )
    }
}

impl Symbol {
    /// Creates a new [Symbol].
    pub fn new(id: usize, kind: SymbolKind) -> Self {
        Self {
            id,
            kind,
            tokens: Vec::new(),
            closed: false,
        }
    }

    /// Determines what the **next** kind of child symbol is expected. See: [`symbol_expectations`]
    pub fn expectation(&self, child_kinds: Vec<SymbolKind>) -> Option<&[SymbolKind]> {
        if let Some(expectations) = symbol_expectations(&self.kind) {
            let len_children = child_kinds.len();
            let len_expectations = expectations.0.len();

            if len_children < len_expectations {
                Some(expectations.0[len_children])
            } else {
                let repeat_final_expectation = {
                    match expectations.1 {
                        Bounds::Open => match self.kind {
                            SymbolKind::Statement => {
                                !self.last_token_kind_eq(TokenKind::StatementEnd)
                            }
                            SymbolKind::Array => !self.last_token_kind_eq(TokenKind::ArrayClose),
                            _ => true,
                        },
                        Bounds::Closed => false,
                    }
                };

                if repeat_final_expectation {
                    Some(expectations.0[len_expectations - 1])
                } else {
                    None
                }
            }
        } else {
            None
        }
    }

    /// Determines if the symbol will take a token of the provided kind. Requires the ability to
    /// look ahead to the next token for some decisions.
    ///
    /// # Errors
    ///
    /// Returns an error if the provided [TokenKind] is indicative of a syntax error.
    pub fn takes_token(
        &self,
        token_kind: &TokenKind,
        next_token_kind: &Option<TokenKind>,
    ) -> Result<bool, String> {
        if self.closed {
            return Ok(false);
        }

        match self.kind {
            SymbolKind::Root => Ok(token_kind == &TokenKind::Newline),
            SymbolKind::Statement => {
                Ok(self.tokens.is_empty() && token_kind == &TokenKind::StatementEnd)
            }
            SymbolKind::Comment => Ok(token_kind == &TokenKind::Comment),
            SymbolKind::Clause => {
                Ok(token_kind == &TokenKind::Whitespace || token_kind == &TokenKind::Newline)
            }
            SymbolKind::Keyword => Ok(self.tokens.is_empty() && token_kind == &TokenKind::Keyword),
            SymbolKind::Array => Ok(match token_kind {
                TokenKind::ArrayOpen => self.tokens.is_empty(),
                TokenKind::ArrayClose | TokenKind::ArrayDelimiter => {
                    !self.tokens.is_empty() && !self.last_token_kind_eq(TokenKind::ArrayClose)
                }
                _ => false,
            }),
            SymbolKind::String => {
                let quoted =
                    self.first_token_kind_eq_any(&[TokenKind::QuoteSingle, TokenKind::QuoteDouble]);
                let quotes_closed = quoted && self.last_token_kind_eq_opt(self.first_token_kind());
                match token_kind {
                    TokenKind::Newline => {
                        if quotes_closed || next_token_kind == &Some(TokenKind::Keyword) {
                            Ok(false)
                        } else {
                            Err("newline forbidden in strings".into())
                        }
                    }
                    TokenKind::Whitespace => {
                        Ok(next_token_kind == &Some(TokenKind::Text) && !quotes_closed)
                    }
                    TokenKind::Text => Ok(!quotes_closed),
                    TokenKind::QuoteSingle | TokenKind::QuoteDouble => {
                        Ok(self.tokens.is_empty() || !quotes_closed)
                    }
                    _ => Ok(quoted && !quotes_closed),
                }
            }
            SymbolKind::Scalar => Ok(match token_kind {
                TokenKind::Number => self.tokens.is_empty(),
                TokenKind::Text => !self.tokens.is_empty(),
                TokenKind::Whitespace => {
                    !self.tokens.is_empty() && next_token_kind == &Some(TokenKind::Text)
                }
                _ => false,
            }),
            _ => Ok(false),
        }
    }

    //
    // UTILITY FUNCTIONS
    //

    /// Returns the kind of the first token.
    fn first_token_kind(&self) -> Option<&TokenKind> {
        match self.tokens.first() {
            Some(token) => Some(&token.kind),
            None => None,
        }
    }

    // fn first_token_kind_eq(&self, kind: TokenKind) -> bool {
    //     match self.first_token_kind() {
    //         Some(last_kind) => last_kind == &kind,
    //         None => false,
    //     }
    // }

    /// Returns whether the first token kind matches any of the provided kinds.
    fn first_token_kind_eq_any(&self, kinds: &[TokenKind]) -> bool {
        match self.first_token_kind() {
            Some(last_kind) => kinds.contains(last_kind),
            None => false,
        }
    }

    /// Returns the kind of the last token.
    fn last_token_kind(&self) -> Option<&TokenKind> {
        match self.tokens.last() {
            Some(token) => Some(&token.kind),
            None => None,
        }
    }

    /// Returns whether the last token kind matches the provided kind.
    fn last_token_kind_eq(&self, kind: TokenKind) -> bool {
        match self.last_token_kind() {
            Some(last_kind) => last_kind == &kind,
            None => false,
        }
    }

    /// Returns whether the last token kind matches the provided kind.
    fn last_token_kind_eq_opt(&self, opt_kind: Option<&TokenKind>) -> bool {
        match self.last_token_kind() {
            Some(last_kind) => match opt_kind {
                Some(kind) => last_kind == kind,
                None => false,
            },
            None => false,
        }
    }

    // fn last_token_kind_eq_any(&self, kinds: &[TokenKind]) -> bool {
    //     match self.last_token_kind() {
    //         Some(last_kind) => kinds.contains(last_kind),
    //         None => false,
    //     }
    // }

    // fn last_child_kind_eq(
    //     &self,
    //     child_kinds: Vec<SymbolKind>,
    //     test_kind: &SymbolKind,
    // ) -> bool {
    //     match child_kinds.last() {
    //         Some(last_kind) => last_kind == test_kind,
    //         None => false,
    //     }
    // }
}
