// SPDX-License-Identifier: GPL-3.0-or-later
use convert_case::{Case, Casing};
use prettytable::{format, row, Cell, Row, Table};
use std::fmt::{self, Display};

/// Kinds of tokens, determined by character type.
#[derive(Debug, Clone, strum_macros::Display, PartialEq, Eq)]
pub enum TokenKind {
    /// Unrecognized characters
    Invalid,

    /// Whitespace characters except carriage return `\r` and line feed `\n`
    Whitespace,

    /// Line endings, either `\r\n` (CRLF) or `\n` (LF)
    Newline,

    /// Hash `#`
    Comment,

    /// Uppercase alphabetic characters
    Keyword,

    /// Lowercase alphabetic characters
    Text,

    /// Numerical characters
    Number,

    /// Period / full stop `.`
    StatementEnd,

    /// Left parenthesis `(`
    ArrayOpen,

    /// Right parenthesis `)`
    ArrayClose,

    /// Comma `,`
    ArrayDelimiter,

    /// Single quotes `\'`
    QuoteSingle,

    /// Double quotes `\"`
    QuoteDouble,
}

impl TokenKind {
    /// Determines what kind of token a character corresponds to.
    pub fn from_char(c: char) -> Self {
        if c.is_whitespace() {
            match c {
                '\r' | '\n' => Self::Newline,
                _ => Self::Whitespace,
            }
        } else if c.is_alphabetic() {
            if c.is_uppercase() {
                Self::Keyword
            } else {
                Self::Text
            }
        } else if c.is_numeric() {
            Self::Number
        } else {
            match c {
                '#' => Self::Comment,
                '.' => Self::StatementEnd,
                '(' => Self::ArrayOpen,
                ')' => Self::ArrayClose,
                ',' => Self::ArrayDelimiter,
                '\'' => Self::QuoteSingle,
                '\"' => Self::QuoteDouble,
                _ => Self::Invalid,
            }
        }
    }
}

/// A recognized string of contiguous characters.
#[derive(Debug, Clone)]
pub struct Token {
    /// Token kind, aka type.
    pub kind: TokenKind,

    /// Text content of the token.
    pub lexeme: String,

    /// Number of the line the token occurs on.
    pub line: u32,

    /// Number of the column this token occurs in.
    pub column: u16,
}

impl Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let kind_str = self.kind.to_string().to_case(Case::UpperSnake);
        write!(
            f,
            "<{} \"{}\" @ {}:{}>",
            kind_str,
            self.lexeme.escape_debug(),
            self.line,
            self.column
        )
    }
}

impl Token {
    /// Creates a new [`Token`].
    pub fn new(kind: TokenKind, c: char, line: u32, col: u16) -> Token {
        Token {
            kind,
            lexeme: String::from(c),
            line,
            column: col,
        }
    }

    /// Determines if the provided [`TokenKind`] is the same kind as ours.
    pub fn kind_eq(&self, kind: TokenKind) -> bool {
        self.kind == kind
    }

    /// Determines if a token can append the provided character to its lexeme.
    pub fn takes_char(&self, c: char) -> bool {
        let kind = TokenKind::from_char(c);
        match self.kind {
            // Conditional
            TokenKind::Comment => kind != TokenKind::Newline,
            TokenKind::Newline => kind == TokenKind::Newline && !self.lexeme.ends_with('\n'),

            // Always append characters of same kind
            TokenKind::Invalid
            | TokenKind::Whitespace
            | TokenKind::Keyword
            | TokenKind::Text
            | TokenKind::Number => self.kind == kind,

            // Never append (default)
            _ => false,
        }
    }

    /// Returns a cloned token with the provided character appended to its
    /// lexeme.
    pub fn append(&self, c: char) -> Self {
        let mut lexeme = self.lexeme.clone();
        lexeme.push(c);

        Token {
            kind: self.kind.clone(),
            lexeme,
            line: self.line,
            column: self.column,
        }
    }
}

/// Tokenizes the input lexeme.
///
/// # Returns
///
/// The resultant tokens.
pub fn tokenize(content: &str) -> Vec<Token> {
    let mut tokens = Vec::<Token>::new();
    let mut line: u32 = 1;
    let mut col: u16 = 0;

    for c in content.chars() {
        let kind = TokenKind::from_char(c);
        col += 1;

        let push = match tokens.last_mut() {
            Some(prev) => {
                if prev.takes_char(c) {
                    *prev = prev.append(c);
                    false
                } else {
                    true
                }
            }
            None => true,
        };

        if push {
            tokens.push(Token::new(kind.clone(), c, line, col));

            if let TokenKind::Newline = kind {
                line += 1;
                col = 0;
            };
        }
    }

    tokens
}

/// Prints [Token]s as an easily readable table.
pub fn print_token_table(tokens: impl IntoIterator<Item = Token>, include_whitespace: bool) {
    let mut table = Table::new();
    table.set_format(*format::consts::FORMAT_CLEAN);
    table.set_titles(row!["Kind", "Lexeme", "Ln:Col"]);
    table.add_row(row![" ", " ", " "]);

    for token in tokens
        .into_iter()
        .filter(|t| !t.kind_eq(TokenKind::Whitespace) || include_whitespace)
    {
        let kind_str = token.kind.to_string().to_case(Case::UpperSnake);
        table.add_row(Row::new(vec![
            Cell::new(kind_str.as_str()),
            Cell::new(format!("\"{}\"", &token.lexeme.escape_debug()).as_str()),
            Cell::new(format!("{}:{}", &token.line, &token.column).as_str()),
        ]));
    }
    table.printstd();
}
